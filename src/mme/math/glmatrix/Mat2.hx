package mme.math.glmatrix;

import mme.math.glmatrix.Mat2Tools;

#if lime
import lime.utils.Float32Array;
abstract Mat2(Float32Array) from Float32Array to Float32Array {
#else
import haxe.ds.Vector;
abstract Mat2(Vector<Float>) from Vector<Float> to Vector<Float> {
#end
    public inline function new() {
        #if lime
        this = new Float32Array(4);
        #else
        this = new Vector<Float>(4);
        this[1] = 0.0;
        this[2] = 0.0;
        #end
        this[0] = 1.0;
        this[3] = 1.0;
    }
    @:arrayAccess
    public inline function getk(key:Int) {
    return this[key];
    }
    @:arrayAccess
    public inline function setk(k:Int, v:Float):Float {
        this[k] = v;
        return v;
    }
    @:from
    static public function fromArray(a:Array<Float>) {
        return fromValues( a[0], a[1], a[2], a[3] );
    }
    @:to
    public function toArray() {
        return [this[0], this[1], this[2], this[3]];
    }
    public function toString() : String {
        return Mat2Tools.str(this);
    }

    /**
    * Create a new Mat2 with the given values
    *
    * @param m00 Component in column 0, row 0 position (index 0)
    * @param m01 Component in column 0, row 1 position (index 1)
    * @param m10 Component in column 1, row 0 position (index 2)
    * @param m11 Component in column 1, row 1 position (index 3)
    * @returns out A new 2x2 matrix
    */
    public static function fromValues( ?out : Mat2, m00 : Float, m01 : Float, m10 : Float, m11 : Float ) : Mat2 {
        if( out == null ) out = Mat2Tools.create();
        out[0] = m00;
        out[1] = m01;
        out[2] = m10;
        out[3] = m11;
        return out;
    }

    /**
    * Creates a matrix from a given angle
    * This is equivalent to (but much faster than):
    *
    *     Mat2.identity(dest);
    *     Mat2.rotate(dest, dest, rad);
    *
    * @param out Mat2 receiving operation result
    * @param rad the angle to rotate the matrix by
    * @returns out
    */
    public static function fromRotation( ?out : Mat2, rad : Float ) : Mat2 {
        if( out == null ) out = Mat2Tools.create();
        var s = Math.sin(rad);
        var c = Math.cos(rad);
        out[0] = c;
        out[1] = s;
        out[2] = -s;
        out[3] = c;
        return out;
    }

    /**
    * Creates a matrix from a vector scaling
    * This is equivalent to (but much faster than):
    *
    *     Mat2.identity(dest);
    *     Mat2.scale(dest, dest, vec);
    *
    * @param out Mat2 receiving operation result
    * @param v Scaling vector
    * @returns out
    */
    public static function fromScaling( ?out : Mat2, v : Vec2 ) : Mat2 {
        if( out == null ) out = Mat2Tools.create();
        out[0] = v[0];
        out[1] = 0;
        out[2] = 0;
        out[3] = v[1];
        return out;
    }
}
