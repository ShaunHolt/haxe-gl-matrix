package mme.math.glmatrix;

import mme.math.glmatrix.GLMatrix;

import mme.math.glmatrix.Mat2;
import mme.math.glmatrix.Vec2;

/**
 * 2x2 Matrix
 * @module Mat2
 */
class Mat2Tools {

/*

//
// GENERATORS in Mat2 class
//

Mat2.fromValues( ?out : Mat2, m00 : Float, m01 : Float, m10 : Float, m11 : Float ) : Mat2;
Mat2.fromRotation( ?out : Mat2, rad : Float ) : Mat2;
Mat2.fromScaling( ?out : Mat2, v : Vec2 ) : Mat2;


//
// GENERATORS
//
create() : Mat2;
mat2.clone( a : Mat2 ) : Mat2;
?mat2.identity( ?out : Mat2 ) : Mat2;


//
// EDIT
//
mat2.set( out : Mat2, m00 : Float, m01 : Float, m10 : Float, m11 : Float ) : Mat2;


//
// OPERATORS
//

// unary
//
mat2.copy( a : Mat2, ?out : Mat2 ) : Mat2;
mat2.transpose( a : Mat2, ?out : Mat2 ) : Mat2;
mat2.invert( a : Mat2, ?out : Mat2 ) : Mat2;
mat2.adjoint( a : Mat2, ?out : Mat2 ) : Mat2;


// unary, non-Mat2 result
//
mat2.determinant( a : Mat2 ) : Float;
mat2.frob( a : Mat2 ) : Float;
mat2.str( a : Mat2 ) : String;


// binary
//
mat2.multiply( a : Mat2, b : Mat2, ?out : Mat2 ) : Mat2;
mat2.add( a : Mat2, b : Mat2, ?out : Mat2 ) : Mat2;
mat2.subtract( a : Mat2, b : Mat2, ?out : Mat2 ) : Mat2;


// binary, non-Mat2 arg
//
mat2.rotate( a : Mat2, rad : Float, ?out : Mat2 ) : Mat2;
mat2.scale( a : Mat2, v : Vec2, ?out : Mat2 ) : Mat2;
mat2.multiplyScalar( a : Mat2, b : Float, ?out : Mat2 ) : Mat2;
mat2.multiplyVec2( a : Mat2, v : Vec2, ?out : Vec2 ) : Vec2;


// binary, non-Mat2 result
//
mat2.exactEquals( a : Mat2, b : Mat2 ) : Bool;
mat2.equals( a : Mat2, b : Mat2 ) : Bool;


// ternary
//
mat2.multiplyScalarAndAdd( a : Mat2, b : Mat2, scale : Float, ?out : Mat2 ) : Mat2;



// MISC
//
mat2.LDU( a : Mat2, L : Mat2, D : Mat2, U : Mat2 ) : Array<Mat2>;

*/

    /**
    * Creates a new identity Mat2
    *
    * @returns {Mat2} a new 2x2 matrix
    */
    public static inline function create() : Mat2 {
    var out = new Mat2();
    return out;
    }

    /**
    * Creates a new Mat2 initialized with values from an existing matrix
    *
    * @param {Mat2} a matrix to clone
    * @returns {Mat2} a new 2x2 matrix
    */
    public static function clone( a : Mat2 ) : Mat2 {
    var out = new Mat2();
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    return out;
    }

    /**
    * Copy the values from one Mat2 to another
    *
    * @param {Mat2} out the receiving matrix
    * @param {Mat2} a the source matrix
    * @returns {Mat2} out
    */
    public static function copy( a : Mat2, ?out : Mat2 ) : Mat2 {
    if( out == null ) out = create();
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    return out;
    }

    /**
    * Set a Mat2 to the identity matrix
    *
    * @param {Mat2} out the receiving matrix
    * @returns {Mat2} out
    */
    public static function identity( ?out : Mat2 ) : Mat2 {
    if( out == null ) out = create();
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 1;
    return out;
    }

    /**
    * Set the components of a Mat2 to the given values
    *
    * @param {Mat2} out the receiving matrix
    * @param {Float} m00 Component in column 0, row 0 position (index 0)
    * @param {Float} m01 Component in column 0, row 1 position (index 1)
    * @param {Float} m10 Component in column 1, row 0 position (index 2)
    * @param {Float} m11 Component in column 1, row 1 position (index 3)
    * @returns {Mat2} out
    */
    public static function set( out : Mat2, m00 : Float, m01 : Float, m10 : Float, m11 : Float ) : Mat2 {
    out[0] = m00;
    out[1] = m01;
    out[2] = m10;
    out[3] = m11;
    return out;
    }

    /**
    * Transpose the values of a Mat2
    *
    * @param {Mat2} out the receiving matrix
    * @param {Mat2} a the source matrix
    * @returns {Mat2} out
    */
    public static function transpose( a : Mat2, ?out : Mat2 ) : Mat2 {
    if( out == null ) out = create();
    // If we are transposing ourselves we can skip a few steps but have to cache
    // some values
    if (out == a) {
        var a1 = a[1];
        out[1] = a[2];
        out[2] = a1;
    } else {
        out[0] = a[0];
        out[1] = a[2];
        out[2] = a[1];
        out[3] = a[3];
    }

    return out;
    }

    /**
    * Inverts a Mat2
    *
    * @param {Mat2} out the receiving matrix
    * @param {Mat2} a the source matrix
    * @returns {Mat2} out
    */
    public static function invert( a : Mat2, ?out : Mat2 ) : Mat2 {
    if( out == null ) out = create();
    var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3];

    // Calculate the determinant
    var det = a0 * a3 - a2 * a1;

    if ( det == 0.0 ) {
        return null;
    }
    det = 1.0 / det;

    out[0] =  a3 * det;
    out[1] = -a1 * det;
    out[2] = -a2 * det;
    out[3] =  a0 * det;

    return out;
    }

    /**
    * Calculates the adjugate of a Mat2
    *
    * @param {Mat2} out the receiving matrix
    * @param {Mat2} a the source matrix
    * @returns {Mat2} out
    */
    public static function adjoint( a : Mat2, ?out : Mat2 ) : Mat2 {
    if( out == null ) out = create();
    // Caching this value is nessecary if out == a
    var a0 = a[0];
    out[0] =  a[3];
    out[1] = -a[1];
    out[2] = -a[2];
    out[3] =  a0;

    return out;
    }

    /**
    * Calculates the determinant of a Mat2
    *
    * @param {Mat2} a the source matrix
    * @returns {Float} determinant of a
    */
    public static function determinant( a : Mat2 ) : Float {
    return a[0] * a[3] - a[2] * a[1];
    }

    /**
    * Multiplies two Mat2's
    *
    * @param {Mat2} out the receiving matrix
    * @param {Mat2} a the first operand
    * @param {Mat2} b the second operand
    * @returns {Mat2} out
    */
    public static function multiply( a : Mat2, b : Mat2, ?out : Mat2 ) : Mat2 {
    if( out == null ) out = create();
    var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3];
    var b0 = b[0], b1 = b[1], b2 = b[2], b3 = b[3];
    out[0] = a0 * b0 + a2 * b1;
    out[1] = a1 * b0 + a3 * b1;
    out[2] = a0 * b2 + a2 * b3;
    out[3] = a1 * b2 + a3 * b3;
    return out;
    }

    /**
    * Rotates a Mat2 by the given angle
    *
    * @param {Mat2} out the receiving matrix
    * @param {Mat2} a the matrix to rotate
    * @param {Float} rad the angle to rotate the matrix by
    * @returns {Mat2} out
    */
    public static function rotate( a : Mat2, rad : Float, ?out : Mat2 ) : Mat2 {
    if( out == null ) out = create();
    var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3];
    var s = Math.sin(rad);
    var c = Math.cos(rad);
    out[0] = a0 *  c + a2 * s;
    out[1] = a1 *  c + a3 * s;
    out[2] = a0 * -s + a2 * c;
    out[3] = a1 * -s + a3 * c;
    return out;
    }

    /**
    * Scales the Mat2 by the dimensions in the given vec2
    *
    * @param {Mat2} out the receiving matrix
    * @param {Mat2} a the matrix to rotate
    * @param {vec2} v the vec2 to scale the matrix by
    * @returns {Mat2} out
    **/
    public static function scale( a : Mat2, v : Vec2, ?out : Mat2 ) : Mat2 {
    if( out == null ) out = create();
    var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3];
    var v0 = v[0], v1 = v[1];
    out[0] = a0 * v0;
    out[1] = a1 * v0;
    out[2] = a2 * v1;
    out[3] = a3 * v1;
    return out;
    }

    /**
    * Returns a string representation of a Mat2
    *
    * @param {Mat2} a matrix to represent as a string
    * @returns {String} string representation of the matrix
    */
    public static function str( a : Mat2 ) : String {
    return 'Mat2(' + a[0] + ', ' + a[1] + ', ' + a[2] + ', ' + a[3] + ')';
    }

    /**
    * Returns Frobenius norm of a Mat2
    *
    * @param {Mat2} a the matrix to calculate Frobenius norm of
    * @returns {Float} Frobenius norm
    */
    public static function frob( a : Mat2 ) : Float {
    return(Math.sqrt(Math.pow(a[0], 2) + Math.pow(a[1], 2) + Math.pow(a[2], 2) + Math.pow(a[3], 2)));
    }

    /**
    * Returns L, D and U matrices (Lower triangular, Diagonal and Upper triangular) by factorizing the input matrix
    * @param {Mat2} L the lower triangular matrix
    * @param {Mat2} D the diagonal matrix
    * @param {Mat2} U the upper triangular matrix
    * @param {Mat2} a the input matrix to factorize
    */

    public static function LDU( a : Mat2, L : Mat2, D : Mat2, U : Mat2 ) : Array<Mat2> {
    L[2] = a[2]/a[0];
    U[0] = a[0];
    U[1] = a[1];
    U[3] = a[3] - L[2] * U[1];
    return [L, D, U];
    }

    /**
    * Adds two Mat2's
    *
    * @param {Mat2} out the receiving matrix
    * @param {Mat2} a the first operand
    * @param {Mat2} b the second operand
    * @returns {Mat2} out
    */
    public static function add( a : Mat2, b : Mat2, ?out : Mat2 ) : Mat2 {
    if( out == null ) out = create();
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
    out[3] = a[3] + b[3];
    return out;
    }

    /**
    * Subtracts matrix b from matrix a
    *
    * @param {Mat2} out the receiving matrix
    * @param {Mat2} a the first operand
    * @param {Mat2} b the second operand
    * @returns {Mat2} out
    */
    public static function subtract( a : Mat2, b : Mat2, ?out : Mat2 ) : Mat2 {
    if( out == null ) out = create();
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    out[2] = a[2] - b[2];
    out[3] = a[3] - b[3];
    return out;
    }

    /**
    * Returns whether or not the matrices have exactly the same elements in the same position (when compared with ==)
    *
    * @param {Mat2} a The first matrix.
    * @param {Mat2} b The second matrix.
    * @returns {Boolean} True if the matrices are equal, false otherwise.
    */
    public static function exactEquals( a : Mat2, b : Mat2 ) : Bool {
    return a[0] == b[0] && a[1] == b[1] && a[2] == b[2] && a[3] == b[3];
    }

    /**
    * Returns whether or not the matrices have approximately the same elements in the same position.
    *
    * @param {Mat2} a The first matrix.
    * @param {Mat2} b The second matrix.
    * @returns {Boolean} True if the matrices are equal, false otherwise.
    */
    public static function equals( a : Mat2, b : Mat2 ) : Bool {
    var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3];
    var b0 = b[0], b1 = b[1], b2 = b[2], b3 = b[3];
    return (Math.abs(a0 - b0) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a0), Math.abs(b0))) &&
            Math.abs(a1 - b1) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a1), Math.abs(b1))) &&
            Math.abs(a2 - b2) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a2), Math.abs(b2))) &&
            Math.abs(a3 - b3) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a3), Math.abs(b3))));
    }

    /**
    * Multiply Mat2 with Vec2
    * out = a * v
    *
    * @param {Vec2} out the receiving vector
    * @param {Mat2} a matrix to be multiplied by vector
    * @param {Vec2} v the vector to multiply matrix with
    * @returns {Vec2} out
    */
    public static function multiplyVec2( a : Mat2, v : Vec2, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = Vec2Tools.create();
    var x = v[0],
        y = v[1];
    out[0] = a[0] * x + a[2] * y;
    out[1] = a[1] * x + a[3] * y;
    return out;
    }

    /**
    * Multiply each element of the matrix by a scalar.
    *
    * @param {Mat2} out the receiving matrix
    * @param {Mat2} a the matrix to scale
    * @param {Float} b amount to scale the matrix's elements by
    * @returns {Mat2} out
    */
    public static function multiplyScalar( a : Mat2, b : Float, ?out : Mat2 ) : Mat2 {
    if( out == null ) out = create();
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
    out[3] = a[3] * b;
    return out;
    }

    /**
    * Adds two Mat2's after multiplying each element of the second operand by a scalar value.
    *
    * @param {Mat2} out the receiving vector
    * @param {Mat2} a the first operand
    * @param {Mat2} b the second operand
    * @param {Float} scale the amount to scale b's elements by before adding
    * @returns {Mat2} out
    */
    public static function multiplyScalarAndAdd( a : Mat2, b : Mat2, scale : Float, ?out : Mat2 ) : Mat2 {
    if( out == null ) out = create();
    out[0] = a[0] + (b[0] * scale);
    out[1] = a[1] + (b[1] * scale);
    out[2] = a[2] + (b[2] * scale);
    out[3] = a[3] + (b[3] * scale);
    return out;
    }

    /**
    * Alias for {@link Mat2.multiply}
    * @function
    */
    public static var mul = multiply;

    /**
    * Alias for {@link Mat2.subtract}
    * @function
    */
    public static var sub = subtract;
}
