package mme.math.glmatrix;

import mme.math.glmatrix.Mat3Tools;

#if lime
import lime.utils.Float32Array;
abstract Mat3(Float32Array) from Float32Array to Float32Array {
#else
import haxe.ds.Vector;
abstract Mat3(Vector<Float>) from Vector<Float> to Vector<Float> {
#end
    public inline function new() {
        #if lime
        this = new Float32Array(9);
        #else
        this = new Vector<Float>(9);
        this[1] = 0.0;
        this[2] = 0.0;
        this[3] = 0.0;
        this[5] = 0.0;
        this[6] = 0.0;
        this[7] = 0.0;
        #end
        this[0] = 1.0;
        this[4] = 1.0;
        this[8] = 1.0;
    }
    @:arrayAccess
    public inline function getk(key:Int) {
    return this[key];
    }
    @:arrayAccess
    public inline function setk(k:Int, v:Float):Float {
        this[k] = v;
        return v;
    }
    @:from
    static public function fromArray(a:Array<Float>) {
        return fromValues( a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8] );
    }
    @:to
    public function toArray() {
        return [this[0], this[1], this[2], this[3], this[4], this[5], this[6], this[7], this[8]];
    }
    public function toString() : String {
        return Mat3Tools.str(this);
    }

    /**
    * Create a new Mat3 with the given values
    *
    * @param m00 Component in column 0, row 0 position (index 0)
    * @param m01 Component in column 0, row 1 position (index 1)
    * @param m02 Component in column 0, row 2 position (index 2)
    * @param m10 Component in column 1, row 0 position (index 3)
    * @param m11 Component in column 1, row 1 position (index 4)
    * @param m12 Component in column 1, row 2 position (index 5)
    * @param m20 Component in column 2, row 0 position (index 6)
    * @param m21 Component in column 2, row 1 position (index 7)
    * @param m22 Component in column 2, row 2 position (index 8)
    * @returns A new Mat3
    */
    public static function fromValues( ?out : Mat3, m00 : Float, m01 : Float, m02 : Float, m10 : Float, m11 : Float, m12 : Float, m20 : Float, m21 : Float, m22 : Float ) : Mat3 {
        if( out == null ) out = Mat3Tools.create();
        out[0] = m00;
        out[1] = m01;
        out[2] = m02;
        out[3] = m10;
        out[4] = m11;
        out[5] = m12;
        out[6] = m20;
        out[7] = m21;
        out[8] = m22;
        return out;
    }

    /**
    * Copies the upper-left 3x3 values into the given Mat3.
    *
    * @param out the receiving 3x3 matrix
    * @param a   the source 4x4 matrix
    * @returns out
    */
    public static function fromMat4( ?out : Mat3, a : Mat4 ) : Mat3 {
        if( out == null ) out = Mat3Tools.create();
        out[0] = a[0];
        out[1] = a[1];
        out[2] = a[2];
        out[3] = a[4];
        out[4] = a[5];
        out[5] = a[6];
        out[6] = a[8];
        out[7] = a[9];
        out[8] = a[10];
        return out;
    }

    /**
    * Creates a matrix from a vector translation
    * This is equivalent to (but much faster than):
    *
    *     Mat3.identity(dest);
    *     Mat3.translate(dest, dest, vec);
    *
    * @param out Mat3 receiving operation result
    * @param v Translation vector
    * @returns out
    */
    public static function fromTranslation( ?out : Mat3, v : Vec2 ) : Mat3 {
        if( out == null ) out = Mat3Tools.create();
        out[0] = 1;
        out[1] = 0;
        out[2] = 0;
        out[3] = 0;
        out[4] = 1;
        out[5] = 0;
        out[6] = v[0];
        out[7] = v[1];
        out[8] = 1;
        return out;
    }

    /**
    * Creates a matrix from a given angle
    * This is equivalent to (but much faster than):
    *
    *     Mat3.identity(dest);
    *     Mat3.rotate(dest, dest, rad);
    *
    * @param out Mat3 receiving operation result
    * @param rad the angle to rotate the matrix by
    * @returns out
    */
    public static function fromRotation( ?out : Mat3, rad : Float ) : Mat3 {
        if( out == null ) out = Mat3Tools.create();
        var s = Math.sin(rad), c = Math.cos(rad);

        out[0] = c;
        out[1] = s;
        out[2] = 0;

        out[3] = -s;
        out[4] = c;
        out[5] = 0;

        out[6] = 0;
        out[7] = 0;
        out[8] = 1;
        return out;
    }

    /**
    * Creates a matrix from a vector scaling
    * This is equivalent to (but much faster than):
    *
    *     Mat3.identity(dest);
    *     Mat3.scale(dest, dest, vec);
    *
    * @param out Mat3 receiving operation result
    * @param v Scaling vector
    * @returns out
    */
    public static function fromScaling( ?out : Mat3, v : Vec2 ) : Mat3 {
        if( out == null ) out = Mat3Tools.create();
        out[0] = v[0];
        out[1] = 0;
        out[2] = 0;

        out[3] = 0;
        out[4] = v[1];
        out[5] = 0;

        out[6] = 0;
        out[7] = 0;
        out[8] = 1;
        return out;
    }

    /**
    * Copies the values from a Mat2d into a Mat3
    *
    * @param out the receiving matrix
    * @param a the matrix to copy
    * @returns out
    **/
    public static function fromMat2d( ?out : Mat3, a : Mat2d ) : Mat3 {
        if( out == null ) out = Mat3Tools.create();
        out[0] = a[0];
        out[1] = a[1];
        out[2] = 0;

        out[3] = a[2];
        out[4] = a[3];
        out[5] = 0;

        out[6] = a[4];
        out[7] = a[5];
        out[8] = 1;
        return out;
    }

    /**
    * Calculates a 3x3 matrix from the given quaternion
    *
    * @param out Mat3 receiving operation result
    * @param q Quaternion to create matrix from
    *
    * @returns out
    */
    public static function fromQuat( ?out : Mat3, q : Quat ) : Mat3 {
        if( out == null ) out = Mat3Tools.create();
        var x = q[0], y = q[1], z = q[2], w = q[3];
        var x2 = x + x;
        var y2 = y + y;
        var z2 = z + z;

        var xx = x * x2;
        var yx = y * x2;
        var yy = y * y2;
        var zx = z * x2;
        var zy = z * y2;
        var zz = z * z2;
        var wx = w * x2;
        var wy = w * y2;
        var wz = w * z2;

        out[0] = 1 - yy - zz;
        out[3] = yx - wz;
        out[6] = zx + wy;

        out[1] = yx + wz;
        out[4] = 1 - xx - zz;
        out[7] = zy - wx;

        out[2] = zx - wy;
        out[5] = zy + wx;
        out[8] = 1 - xx - yy;

        return out;
    }
}
