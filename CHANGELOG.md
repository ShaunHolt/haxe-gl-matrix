# HEAD


# v1.0.1

- make `Vec2Tools.rotate()` parameter `b` optional so we don't have to type `a.rotate([0,0],angle)` all the time


# v1.0.0
- Initial release
